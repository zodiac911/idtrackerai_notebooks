import cv2
import numpy as np
from tqdm import tqdm
images = np.load('images.npy')
# Release everything if job is finished
fourcc = cv2.VideoWriter_fourcc(*'MJPG')
out = cv2.VideoWriter('output_small.avi',fourcc, 32, (190,190),1)
print("saving gray_scale_images")
for focal in range(1):
    print("focal ", focal)
    for i, image in tqdm(enumerate(images[:, focal, ...]), 'saving_video'):
        image = cv2.cvtColor(image,cv2.COLOR_GRAY2BGR)
        image[image<240] = 155
        image[image>240] = 0
        image[image==155] = 255
        out.write(image)

out.release()
cv2.destroyAllWindows()

# Release everything if job is finished
# fourcc = cv2.VideoWriter_fourcc(*'XVID')
#out = cv2.VideoWriter('output_binary.avi',fourcc, 32, (190,190))
#print("saving binarized_images")
#for focal in range(images.shape[1]):
#    print("focal ", focal)
#    for i, image in tqdm(enumerate(images[:, focal, ...]), 'saving_video'):
#        image = cv2.cvtColor(image,cv2.COLOR_GRAY2BGR)
#        image[image>=249] = 0
#        image[image!=0] = 255
#        out.write(image)
#
#out.release()
#cv2.destroyAllWindows()