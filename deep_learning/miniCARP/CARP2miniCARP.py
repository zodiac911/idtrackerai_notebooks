import numpy as np
import matplotlib.pyplot as plt

images = np.load('CARP_images.npy')
labels = np.load('CARP_labels.npy')

individuals = np.arange(0, labels.max(), 5)
print(individuals, len(individuals))

images2 = images[np.where(labels%5 == 0)].astype(np.uint8)
labels2 = labels[np.where(labels%5 == 0)]
labels2 = (labels2//5).astype(np.uint8)

print(images2.shape, labels2.shape)
unique, counts = np.unique(labels2, return_counts=True)
print(dict(zip(unique, counts)))

print(images2[0].max(), images2[0].min(), images.dtype)
print(labels2[0], labels2.dtype)

np.save('miniCARP_images.npy', images2)
np.save('miniCARP_labels.npy', labels2)

