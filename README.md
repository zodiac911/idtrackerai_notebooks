# idtracker.ai notebooks

This repository includes IPython notebooks and python scripts that we have created
for different courses where we explained and used [idtracker.ai](idtracker.ai).

The folder *deep_learning* includes notebooks that explain
some basics on machine learning, neural networks and convolutional neural networks.
In particular, one of the notebooks applies convolutional neural networks to identify
images of juvenile zebrafish as we do in [idtracker.ai](idtracker.ai).

The folder *trajectories_analysis* includes notebooks with examples on how to perform
different types of analysis on the trajectories obtained with [idtracker.ai](idtracker.ai).
These notebooks are based on [trajectorytools](https://github.com/fjhheras/trajectorytools),
a python toolbox developed by Francisco J.H. Heras to analysis trajectories of animal collectives.

The folder *idtrackerai_objects_analysis* includes a notebook to analyze fish midlines using the module [in this GitLab repository](https://gitlab.com/polavieja_lab/midline)

The folder *data* includes data needed to run the different notebooks.

If you use any code from this repository please reference the article

**[1] Romero-Ferrero, F., Bergomi, M.G., Hinz, R.C., Heras, F.J.H., de Polavieja, G.G., Nature Methods, 2019.
idtracker.ai: tracking all individuals in small or large collectives of unmarked animals.**

*F.R.-F. and M.G.B. contributed equally to this work. Correspondence should be addressed to G.G.d.P:
gonzalo.polavieja@neuro.fchampalimaud.org.*
